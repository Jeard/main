#include "config.h"
#include "Functions.h"
#include "Active_CAN.h"
#include "CAN.h"


void setup() {
  Initiate();
  delay(1000);
  SelectCan(2);
}

void loop() {
  if (!SerialBT.hasClient()) {
    digitalWrite(LED2, HIGH);
    while (!SerialBT.hasClient()) {
    delay(100);
      }
  Serial.println(encrypted_ID_arr);
  SerialBT.println(encrypted_ID_arr);
  digitalWrite(LED2, LOW);
  delay(500);
    }
    digitalWrite(LED1, LOW);
  if ((xSemaphoreTake(timerSemaphore, 0) == pdTRUE)) {
    isrCounter++;
    SelectCan(1);
    while (CheckFlags(F_0x1db | F_0x1dc | F_0x55b | F_0x5bc ) != 0) {
      can.AcquireEvData();
    }
    SelectCan(2);
      while (CheckFlags(F_0x002 | F_0x176 | F_0x180 | F_0x1cb | F_0x1d5 | F_0x260 | F_0x284 | F_0x285 | F_0x292 | F_0x355 | F_0x5b3 | F_0x5c5)!=0) {
      can.AcquireCarData();
      }
    if (isrCounter == 10) {
      SelectCan(1);
      for (;;) {
        if (CheckFlags(F_ActiveData) == 0) {
          break;
        }
        can.AcquireActiveData();
      }
      isrCounter = 0;
    }
    can.ProcessData();
   can.PublishData();  
 }
  digitalWrite(LED1, HIGH);
}




