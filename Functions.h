void IRAM_ATTR onTimer() {
  xSemaphoreGiveFromISR(timerSemaphore, NULL);
}

void SetFlag(uint32_t bits) {
  Flags |= bits;
}

void ClearFlag(uint32_t bits) {
  Flags &= ~bits;
}

uint32_t CheckFlags(uint32_t bits) {
  return (bits - (Flags &= bits));
}

#if defined (PCB)
void SelectCan(uint8_t can) {
  switch (can) {
    case 0:
      digitalWrite(EN1, HIGH);
      digitalWrite(EN2, HIGH);
      break;
    case 1:
      digitalWrite(EN1, HIGH);
      digitalWrite(EN2, LOW);
      break;
    case 2:
      digitalWrite(EN1, LOW);
      digitalWrite(EN2, HIGH);
      break;
  }
}
#else
void SelectCan(uint8_t can) {
  switch (can) {
    case 0:
      digitalWrite(EN1, HIGH);
      digitalWrite(EN2, HIGH);
      break;
    case 1:
      digitalWrite(EN1, LOW);
      digitalWrite(EN2, HIGH);
      break;
    case 2:
      digitalWrite(EN1, HIGH);
      digitalWrite(EN2, LOW);
      break;
  }
}
#endif

void Initiate() {
  timerSemaphore = xSemaphoreCreateBinary();
  timer = timerBegin(2, 80, true);
  timerAttachInterrupt(timer, &onTimer, true);
  timerAlarmWrite(timer, 1000000, true);
  timerAlarmEnable(timer);
  pinMode(EN1, OUTPUT);
  pinMode(EN2, OUTPUT);
  pinMode(LED1, OUTPUT);
  pinMode(LED2, OUTPUT);
  SelectCan(0);
  Serial.begin(115200);
  chipid_L = ESP.getEfuseMac();
  chipid_S = (uint32_t) ESP.getEfuseMac();
  sprintf(Device_ID, "EM-000%i", chipid_S);
  sprintf(chip_ID, "%i%i", (uint16_t)(chipid_L >> 32), (uint32_t)(chipid_L));
  StaticJsonBuffer<100> ID_buff;
  JsonObject& IDMsg = ID_buff.createObject();
  IDMsg["Chip_ID"]  = chip_ID;
  size_t len = (IDMsg.measureLength() + 1);
  char* ID_arr = (char*) malloc(len);
  IDMsg.printTo(ID_arr, len);
  ID_buff.clear();
  char*DviceID_64    = aesB64.encry_arr2arr(Device_ID, Key);
  encrypted_ID_arr = aesB64.encry_arr2arr(ID_arr, Key);
  SerialBT.begin(DviceID_64);
  Serial.println(DviceID_64);
  Serial.println(F("LeafCan Initiated..."));
  digitalWrite(LED2, LOW);
  while (!SerialBT.hasClient()) {
     digitalWrite(LED2, HIGH);
     delay(500);
     digitalWrite(LED2, LOW);
    }
  digitalWrite(LED2, LOW);
  Serial.println(F("connected"));
  SerialBT.flush();
  Serial.flush();
  Serial.println(encrypted_ID_arr);
  SerialBT.println(encrypted_ID_arr);
  CAN_cfg.speed = CAN_SPEED_500KBPS;
  CAN_cfg.tx_pin_id = TX;
  CAN_cfg.rx_pin_id = RX;
  CAN_cfg.rx_queue = xQueueCreate(5, sizeof(CAN_frame_t));
  SelectCan(1);
  ESP32Can.CANInit();
  Flags = 0;
}



