
void Active_Can :: SetFrameAmount() {
  switch (group) {
    case 1:
      Requird_Frames = Group1_frames;
      break;
    case 2:
      Requird_Frames = Group2_frames;
      break;
    case 4:
      Requird_Frames = Group4_frames;
      break;
  }
}

void Active_Can :: RequstInitialFrame(uint8_t Grp) {
  frame.FIR.B.FF = CAN_frame_std;
  frame.MsgID = 0x79b;
  frame.FIR.B.DLC = 8;
  frame.data.u8[0] = 0x02;
  frame.data.u8[1] = 0x21;
  frame.data.u8[2] = Grp;
  frame.data.u8[3] = 0x0;
  frame.data.u8[4] = 0x0;
  frame.data.u8[5] = 0x0;
  frame.data.u8[6] = 0x0;
  frame.data.u8[7] = 0x0;
  ESP32Can.CANWriteFrame(&frame);
}

void Active_Can :: RequstNextFrame() {
  frame.FIR.B.FF = CAN_frame_std;
  frame.MsgID = 0x79b;
  frame.FIR.B.DLC = 8;
  frame.data.u8[0] = 0x30;
  frame.data.u8[1] = 0x01;
  frame.data.u8[2] = 0x00;
  frame.data.u8[3] = 0xff;
  frame.data.u8[4] = 0xff;
  frame.data.u8[5] = 0xff;
  frame.data.u8[6] = 0xff;
  frame.data.u8[7] = 0xff;
  ESP32Can.CANWriteFrame(&frame);
}

void Active_Can :: printframe() {
  printf("%x ", frame.MsgID);
  for (int i = 0; i < 8; i++) {
    printf("%x ", frame.data.u8[i]);
  }
  printf("\n");
}

void Active_Can :: Acquire0x79bData() {
  SetFrameAmount();
  RequstInitialFrame(group);
  for (;;) {
    if (frame.MsgID == 0x7bb) {
      break;
    }
    xQueueReceive(CAN_cfg.rx_queue, &frame, 10 * portTICK_PERIOD_MS);
  }
  switch (group) {
    case 1:
      break;
    case 2:
      for (uint8_t i = 4; i < 8; i++) {
        CellPairRaw[count] = frame.data.u8[i];
        count++;
      }
      break;
    case 4:
      BatTemp [0] = frame.data.u8[6];
      break;
  }
  for (;;) {
    if (Current_Frame >= Requird_Frames) {
      break;
    }
    Current_Frame++;
    RequstNextFrame();
    for (;;) {
      if (frame.MsgID == 0x7bb) {
        break;
      }
      xQueueReceive(CAN_cfg.rx_queue, &frame, 0 * portTICK_PERIOD_MS);
    }
    switch (group) {
      case 1:
        if (frame.data.u8[0] == 0x24) {
          SOH = ((frame.data.u8[2] << 8) | frame.data.u8[3]);
          HRSOC = ((frame.data.u8[5] << 16) | (frame.data.u8[6] << 8) | frame.data.u8[7]);
        }
        else if ((frame.data.u8[0] == 0x25)) {
          Cap=0;
          Cap = ((frame.data.u8[2] << 16) | (frame.data.u8[3] << 8) | frame.data.u8[4]);
        }
        break;

      case 2:
        for (uint8_t i = 1; i < 8; i++) {
          if (count <= 191) {
            CellPairRaw[count] = frame.data.u8[i];
            count++;
          }
        }
        break;
      case 4:
        if (frame.data.u8[0] == 0x21) {
          BatTemp [1] = frame.data.u8[2];
          BatTemp [2] = frame.data.u8[5];
        }
        else if (frame.data.u8[0] == 0x22) {
          BatTemp [3] = frame.data.u8[1];
        }
        break;
    }
  }
  Current_Frame = 1;
  switch (group) {
    case 1:
      group = 2;
      break;
    case 2:
      group = 4;
      break;
    case 4:

      group = 1;
      uint8_t j = 0;
      for (uint8_t i = 0; i <= 191; i = i + 2) {
        CellPair[j] = ((CellPairRaw[i] << 8) | CellPairRaw[i + 1]);
        j++;
      }
      memset(CellPairRaw, 0, sizeof(CellPairRaw));
      count=0;
      SetFlag(F_ActiveData);
      break;
  }
}



