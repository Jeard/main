void Can::AcquireEvData() {
  if (xQueueReceive(CAN_cfg.rx_queue, &frame, 10 * portTICK_PERIOD_MS) == pdTRUE) {
    uint16_t Id = frame.MsgID;
    int16_t PackCurrent_raw = 0;
    int32_t Volts10 = 0;
    int32_t Current10 = 0;
    switch (Id) {
      case 0x1db:
        PackCurrent_raw = (frame.data.u8[0] << 3) | (frame.data.u8[1] >> 5);
        if (PackCurrent_raw & 0x0400) { // negative - cvt extend sign bit
          PackCurrent_raw |= 0xf800;
        }
        Rawdata.PackCurrent = -PackCurrent_raw;
        Rawdata.PackVoltage = (frame.data.u8[2] << 2) | (frame.data.u8[3] >> 6);
        Volts10 = ((((int32_t)Rawdata.PackVoltage) / 2) * 10) + ((Rawdata.PackVoltage & 1) ? 5 : 0);
        Current10 = ((((int32_t)PackCurrent_raw) / 2) * 10 ) + ((PackCurrent_raw & 1) ? 5 : 0);
        Rawdata.Watt = ((Volts10 * -Current10) + ((Current10 >= 0) ? -50 : 50)) / 100;
        SetFlag(F_0x1db);
        break;
      case 0x1dc:
        Rawdata.AvlbKW = (frame.data.u8[0]);
        SetFlag(F_0x1dc);
        break;
      case 0x55b:
        Rawdata.SOC = (frame.data.u8[0] << 2) | (frame.data.u8[1] >> 6);
        SetFlag(F_0x55b);
        break;
      case 0x5bc:
        Rawdata.Gids = (frame.data.u8[0] << 2) | (frame.data.u8[1] >> 6);
        Rawdata.WattHours = ((((int32_t)Rawdata.Gids) * KW_FACTOR) + 50) / 100;
        SetFlag(F_0x5bc);
        break;
    }
  }
}
void Can::AcquireCarData() {
  if (xQueueReceive(CAN_cfg.rx_queue, &frame, 10 * portTICK_PERIOD_MS) == pdTRUE) {
    uint16_t Id = frame.MsgID;
    switch (Id) {
      case 0x002:
          Rawdata.StinAng = ((frame.data.u8[0] << 8) | frame.data.u8[1]);
          Rawdata.dStinAng = frame.data.u8[2];
          SetFlag(F_0x002);
        break;
      case 0x176:
          Rawdata.speed1 = ((frame.data.u8[0] << 8) | frame.data.u8[1]);
          Rawdata.speed2 = ((frame.data.u8[2] << 8) | frame.data.u8[3]);
          SetFlag(F_0x176);
        break;
      case 0x180:
          Rawdata.MotorAmp = (frame.data.u8[2] << 4) | (frame.data.u8[3] >> 4 );
          if (Rawdata.MotorAmp & 0x800) { // negative - cvt extend sign bit
            Rawdata.MotorAmp |= 0xf000;
          }
          Rawdata.MotorAmp = -Rawdata.MotorAmp;
          Rawdata.ThrottlePosition = frame.data.u8[5];
          SetFlag(F_0x180);
        break;
      case 0x1cb:
          Rawdata.TargetRegenBraking = (frame.data.u8[0] << 3) | (frame.data.u8[1] >> 5);
          Rawdata.TargetBraking = (frame.data.u8[2] << 3) | (frame.data.u8[3] >> 5);
          SetFlag(F_0x1cb);
        break;
      case 0x1d5:
          Rawdata.AppliedRegenBraking = (frame.data.u8[0] << 3) | (frame.data.u8[1] >> 5);
          SetFlag(F_0x1d5);
        break;
      case 0x260:
          Rawdata.AvailablePower = frame.data.u8[0];
          Rawdata.AvailableRegen = frame.data.u8[1];
          SetFlag(F_0x260);
        break;
      case 0x284:
          Rawdata.FrontRightSpeed = (frame.data.u8[0] << 8) | (frame.data.u8[1]);
          Rawdata.FrontLeftSpeed = (frame.data.u8[2] << 8) | (frame.data.u8[3]);
          SetFlag(F_0x284);
        break;
      case 0x285:
          Rawdata.RearRightSpeed = (frame.data.u8[0] << 8) | (frame.data.u8[1]);
          Rawdata.RearLeftSpeed = (frame.data.u8[2] << 8) | (frame.data.u8[3]);
          SetFlag(F_0x285);
        break;
      case 0x292:
          Rawdata.Voltag12V = frame.data.u8[3];
          Rawdata.FrictionBrakePressure = frame.data.u8[6];
          SetFlag(F_0x292);
        break;
      case 0x355:
          Rawdata.speed3 = ((frame.data.u8[0] << 8) | frame.data.u8[1]);
          Rawdata.speed4 = ((frame.data.u8[2] << 8) | frame.data.u8[3]);
          SetFlag(F_0x355);
        break;
      case 0x5b3:
          Rawdata.PackTemperature = frame.data.u8[0];
          Rawdata.SOH = frame.data.u8[1];
          SetFlag(F_0x5b3);
        break;
      case 0x5c5:
          Rawdata.Odometer = ((frame.data.u8[0] << 16) | (frame.data.u8[1] << 8) | frame.data.u8[0]);
          SetFlag(F_0x5c5);
        break;
    }
  }
}

void Can::AcquireActiveData() {
  Active.Acquire0x79bData();
}

void Can::Watt(int32_t w) {
  if (w >= 10000) {
    w += 50;
    Processeddata.Watts = (w / 1000.0) ;
  }
  else if (w <= -10000) {
    w -= 500;
    Processeddata.Watts = (w / 1000.0);
  }
  else if (w < 0) {
    w -= 50;
    int wd1000 = w / 1000;
    if (wd1000 == 0) {
      Processeddata.Watts = (w / 1000.0);
    }
    else {
      Processeddata.Watts = (w / 1000.0);
    }
  }
  else {
    w += 5;
    Processeddata.Watts = (w / 1000.0);
  }
}
void Can::WattHour(int32_t wh) {
  if (wh >= 10000) {
    wh += 50;
    Processeddata.KWh = (wh / 1000.0);
  }
  else if (wh >= 1000) {
    wh += 5;
    Processeddata.KWh = (wh / 1000.0);
  }
  else {
    Processeddata.KWh = (wh / 1000.0);
  }
}

void Can::ProcessData() {
  Watt(Rawdata.Watt);
  WattHour(Rawdata.WattHours);
  Processeddata.AvlbKW                = (Rawdata.AvlbKW / 1.00)                 ;
  Processeddata.Current               = (Rawdata.PackCurrent / 2.00)            ;
  Processeddata.Voltage               = (Rawdata.PackVoltage / 2.00)            ;
  Processeddata.SoC                   = (Rawdata.SOC / 10.0)                    ;
  Processeddata.Gids                  =  Rawdata.Gids                           ;
  Processeddata.StinAng               = (Rawdata.StinAng / 100.00)              ;
  Processeddata.dStinAng              =  Rawdata.dStinAng                       ;
  Processeddata.speed1                =  Rawdata.speed1                         ;
  Processeddata.speed2                =  Rawdata.speed2                         ;
  Processeddata.ThrottlePosition      = (Rawdata.ThrottlePosition / 10.0)       ;
  Processeddata.MotorAmp              = (Rawdata.MotorAmp / 2.00)               ;
  Processeddata.TargetRegenBraking    =  Rawdata.TargetRegenBraking             ;
  Processeddata.TargetBraking         =  Rawdata.TargetBraking                  ;
  Processeddata.AppliedRegenBraking   =  Rawdata.AppliedRegenBraking            ;
  Processeddata.AvailablePower        =  Rawdata.AvailablePower                 ;
  Processeddata.AvailableRegen        =  Rawdata.AvailableRegen                 ;
  Processeddata.FrontRightSpeed       =  Rawdata.FrontRightSpeed                ;
  Processeddata.FrontLeftSpeed        =  Rawdata.FrontLeftSpeed                 ;
  Processeddata.RearRightSpeed        =  Rawdata.RearRightSpeed                 ;
  Processeddata.RearLeftSpeed         =  Rawdata.RearLeftSpeed                  ;
  Processeddata.Voltag12V             = (Rawdata.Voltag12V / 10.0)              ;
  Processeddata.FrictionBrakePressure =  Rawdata.FrictionBrakePressure          ;
  Processeddata.speed3                =  Rawdata.speed3                         ;
  Processeddata.speed4                =  Rawdata.speed4                         ;
  Processeddata.PackTemperature       = ((Rawdata.PackTemperature -32)/1.8 )  ;
  Processeddata.SOH                   = (Rawdata.SOH / 2.00)                    ;
  Processeddata.Odometer              = (Rawdata.Odometer / 160.93)             ;
  if (CheckFlags(F_ActiveData) == 0) {
    Processeddata.CapAh = (Active.Cap / 10000.00);
  }
}

void Can::PublishData() {
  StaticJsonBuffer<2000> PassiveBuffer;
  JsonObject& PassiveMsg = PassiveBuffer.createObject();
  PassiveMsg["SOC"]      = Processeddata.SoC                    ;
  PassiveMsg["Gids"]     = Processeddata.Gids                   ;
  PassiveMsg["TotalKWh"] = Processeddata.KWh                    ;
  PassiveMsg["AvlbKW"]   = Processeddata.AvlbKW                 ;
  PassiveMsg["PackV"]    = Processeddata.Voltage                ;
  PassiveMsg["Amp"]      = Processeddata.Current                ;
  PassiveMsg["KW"]       = Processeddata.Watts                  ;
  PassiveMsg["StinAng"]  = Processeddata.StinAng                ;
  PassiveMsg["dStinAng"] = Processeddata.dStinAng               ;
  PassiveMsg["speed1"]   = Processeddata.speed1                 ;
  PassiveMsg["speed2"]   = Processeddata.speed2                 ;
  PassiveMsg["TrotlPos"] = Processeddata.ThrottlePosition       ;
  PassiveMsg["MotorAmp"] = Processeddata.MotorAmp               ;
  PassiveMsg["TarReBrk"] = Processeddata.TargetRegenBraking     ;
  PassiveMsg["TarBrk"]   = Processeddata.TargetBraking          ;
  PassiveMsg["AlbPwr"]   = Processeddata.AvailablePower         ;
  PassiveMsg["AlbRegn"]  = Processeddata.AvailableRegen         ;
  PassiveMsg["FRSpeed"]  = Processeddata.FrontRightSpeed        ;
  PassiveMsg["FLSpeed"]  = Processeddata.FrontLeftSpeed         ;
  PassiveMsg["RRSpeed"]  = Processeddata.RearRightSpeed         ;
  PassiveMsg["RLSpeed"]  = Processeddata.RearLeftSpeed          ;
  PassiveMsg["12VBat"]   = Processeddata.Voltag12V              ;
  PassiveMsg["BrkPress"] = Processeddata.FrictionBrakePressure  ;
  PassiveMsg["Speed3"]   = Processeddata.speed3                 ;
  PassiveMsg["Speed4"]   = Processeddata.speed4                 ;
  PassiveMsg["AvrgPckT"] = Processeddata.PackTemperature        ;
  PassiveMsg["SOH"]      = Processeddata.SOH                    ;
  PassiveMsg["Odometer"] = Processeddata.Odometer               ;

/*PassiveMsg["SOC"]      = 58.5                   ;
  PassiveMsg["Gids"]     = 250                   ;
  PassiveMsg["TotalKWh"] = 15.657                    ;
  PassiveMsg["AvlbKW"]   = 110                ;
  PassiveMsg["PackV"]    = 350.8                ;
  PassiveMsg["Amp"]      = -11.5                ;
  PassiveMsg["KW"]       = -48.243                  ;
  PassiveMsg["StinAng"]  = 320.26                ;
  PassiveMsg["dStinAng"] = 12               ;
  PassiveMsg["speed1"]   = 235                 ;
  PassiveMsg["speed2"]   = 2589            ;
  PassiveMsg["TrotlPos"] = 12.5       ;
  PassiveMsg["MotorAmp"] = -23.4              ;
  PassiveMsg["TarReBrk"] = 310     ;
  PassiveMsg["TarBrk"]   = 250          ;
  PassiveMsg["AlbPwr"]   = 200        ;
  PassiveMsg["AlbRegn"]  = 198         ;
  PassiveMsg["FRSpeed"]  = 6267        ;
  PassiveMsg["FLSpeed"]  = 6254         ;
  PassiveMsg["RRSpeed"]  = 6214         ;
  PassiveMsg["RLSpeed"]  = 6191          ;
  PassiveMsg["12VBat"]   = 12.5            ;
  PassiveMsg["BrkPress"] = 10 ;
  PassiveMsg["Speed3"]   = 1514                 ;
  PassiveMsg["Speed4"]   = 1606                 ;
  PassiveMsg["AvrgPckT"] = 60.1        ;
  PassiveMsg["SOH"]      = 77.12                   ;
  PassiveMsg["Odometer"] = 268210.45              ;*/

  size_t len = (PassiveMsg.measureLength() + 1);
  char* Passive_arr = (char*) malloc(len);
  PassiveMsg.printTo(Passive_arr, len);
  PassiveBuffer.clear();
  //Serial.println(len);
  //Serial.println(Passive_arr);
  //SerialBT.println(Passive_arr);
  char*encrypted_Passive_arr = aesB64.encry_arr2arr(Passive_arr, Key);
  //String encrypted_Passive_str = aesB64.encry_arr2str(Passive_arr,Key);
  //size_t arr_size=encrypted_Passive_str.length();
  //char * buffer = (char *) malloc(arr_size);
  //encrypted_Passive_str.toCharArray(buffer, arr_size);
  //Serial.write((byte*)&encrypted_Passive_arr, strlen(encrypted_Passive_arr));
  //SerialBT.write((byte*)&encrypted_Passive_arr, strlen(encrypted_Passive_arr));
  Serial.println(encrypted_Passive_arr);
  SerialBT.println(encrypted_Passive_arr);
  free(Passive_arr);
  free(encrypted_Passive_arr);

  if (CheckFlags(F_ActiveData) == 0) {
  //if (isrCounter == 10) {
    isrCounter = 0;
    StaticJsonBuffer<2000> ActiveBuffer;
    JsonObject& ActiveMsg = ActiveBuffer.createObject();
    ActiveMsg ["AtvSOH"]  = Active.SOH;
    ActiveMsg ["HrSOC"]   = Active.HRSOC;
    ActiveMsg ["Ah"]      = Processeddata.CapAh;
    JsonArray& BattTemp   = ActiveMsg.createNestedArray("BattTemp");
    for (uint8_t i = 0; i <= 3; i++) {
      BattTemp.add(Active.BatTemp[i]);
    }
    JsonArray& CellPairV = ActiveMsg.createNestedArray("CellPairV");
    for (uint8_t i = 0; i <= 95; i++) {
      CellPairV.add(Active.CellPair[i]);
    }
    memset(Active.BatTemp, 0, sizeof(Active.BatTemp));
    memset(Active.CellPair, 0, sizeof(Active.CellPair));
    size_t len = (ActiveMsg.measureLength() + 1);
    char* Active_arr = (char*) malloc(len);
    ActiveMsg.printTo(Active_arr, len);
    ActiveBuffer.clear();
    char*encrypted_Active_arr = aesB64.encry_arr2arr(Active_arr, Key);
    delay(200);
    //Serial.println(len);
    //Serial.println(Active_arr);
    //SerialBT.println(Active_arr);
    //Serial.write((byte*)&encrypted_Active_arr, strlen(encrypted_Active_arr));
    //SerialBT.write((byte*)&encrypted_Active_arr, strlen(encrypted_Active_arr));
    //Serial.println(Active_arr);
    //SerialBT.println(Active_arr);
    Serial.println(encrypted_Active_arr);
    SerialBT.println(encrypted_Active_arr);
    free(Active_arr);
    free(encrypted_Active_arr);
  }
  SerialBT.flush();
  Serial.flush();
  Flags = 0;
}

void Can :: printframe() {
  printf("%x ", frame.MsgID);
  for (int i = 0; i < 8; i++) {
    printf("%x ", frame.data.u8[i]);
  }
  printf("\n");
}






