#include <ESP32CAN.h>
#include <CAN_config.h>
#include <ArduinoJson.h>
//#include <Ticker.h>
#include <BluetoothSerial.h>
#include <ESP32AESB64.h>

#define PCB
#if defined(PCB)
#define EN1           25
#define EN2           33
#define TX            GPIO_NUM_32
#define RX            GPIO_NUM_35
#else
#define EN1           25
#define EN2           32
#define TX            GPIO_NUM_33
#define RX            GPIO_NUM_34
#endif

#define LED1          16
#define LED2          17

#define F_0x002       0x00000001
#define F_0x176       0x00000002
#define F_0x180       0x00000004
#define F_0x1cb       0x00000008
#define F_0x1d5       0x00000010
#define F_0x1db       0x00000020
#define F_0x1dc       0x00000040
#define F_0x260       0x00000080
#define F_0x284       0x00000100
#define F_0x285       0x00000200
#define F_0x292       0x00000400
#define F_0x355       0x00000800
#define F_0x55b       0x00001000
#define F_0x5b3       0x00002000
#define F_0x5bc       0x00004000
#define F_0x5c5       0x00008000
#define F_ActiveData  0x00010000
#define F_CycleDone   0x00020000

#define KW_FACTOR     7473

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error
Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

volatile uint32_t Flags;
volatile uint8_t  isrCounter = 0;
volatile          SemaphoreHandle_t timerSemaphore;
uint64_t chipid_L;
uint32_t chipid_S;
char chip_ID [15];
char Device_ID [20];
char Key[] = "emobilityjlpande";
uint8_t CellPairRaw[192];
char*encrypted_ID_arr;



class Active_Can {
  public:
    uint16_t CellPair [96];
    uint16_t BatTemp[4];
    uint16_t SOH;
    uint32_t HRSOC;
    uint32_t Cap;
    void Acquire0x79bData();

  private:
    const uint8_t Group1_frames = 6;
    const uint8_t Group2_frames = 29;
    const uint8_t Group4_frames = 3;
    volatile uint8_t group = 1;
    volatile uint8_t Current_Frame = 1;
    volatile uint8_t Requird_Frames = 0;
    volatile uint8_t count = 0;
    void SetFrameAmount();
    void RequstInitialFrame(uint8_t);
    void RequstNextFrame();
    void printframe();
};


class Can {
  public:
    void   AcquireEvData();
    void   AcquireCarData();
    void   AcquireActiveData();
    void   ProcessData();
    void   PublishData();

  private:
    struct RawData {
      uint16_t SOC                  ;
      uint16_t PackVoltage          ;
      int16_t  PackCurrent          ;
      uint16_t Gids                 ;
      int32_t  Watt                 ;
      uint32_t WattHours            ;
      uint8_t  AvlbKW               ;
      int16_t  StinAng              ;
      uint8_t  dStinAng             ;
      int16_t  speed1               ;
      int16_t  speed2               ;
      uint8_t  ThrottlePosition     ;
      int16_t  MotorAmp             ;
      uint16_t TargetRegenBraking   ;
      uint16_t TargetBraking        ;
      uint16_t AppliedRegenBraking  ;
      uint8_t  AvailablePower       ;
      uint8_t  AvailableRegen       ;
      uint16_t FrontRightSpeed      ;
      uint16_t FrontLeftSpeed       ;
      uint16_t RearRightSpeed       ;
      uint16_t RearLeftSpeed        ;
      uint8_t  Voltag12V            ;
      uint8_t  FrictionBrakePressure;
      uint16_t speed3               ;
      uint16_t speed4               ;
      uint8_t  PackTemperature      ;
      uint8_t  SOH                  ;
      uint32_t Odometer             ;
    } Rawdata;

    struct  ProcessedData {
      float    Current              ;
      float    Voltage              ;
      float    SoC                  ;
      float    KWh                  ;
      float    Watts                ;
      int      Gids                 ;
      float    AvlbKW               ;
      float    CapAh                ;
      float    StinAng              ;
      uint8_t  dStinAng             ;
      float    speed1               ;
      float    speed2               ;
      float    ThrottlePosition     ;
      int16_t  MotorAmp             ;
      uint16_t TargetRegenBraking   ;
      uint16_t TargetBraking        ;
      uint16_t AppliedRegenBraking  ;
      uint8_t  AvailablePower       ;
      uint8_t  AvailableRegen       ;
      uint16_t FrontRightSpeed      ;
      uint16_t FrontLeftSpeed       ;
      uint16_t RearRightSpeed       ;
      uint16_t RearLeftSpeed        ;
      float    Voltag12V            ;
      uint8_t  FrictionBrakePressure;
      uint16_t speed3               ;
      uint16_t speed4               ;
      float    PackTemperature      ;
      float    SOH                  ;
      float    Odometer             ;
    } Processeddata;
    void Watt(int32_t w);
    void WattHour(int32_t wh);
    void printframe();
};

hw_timer_t * timer = NULL;
portMUX_TYPE timerMux = portMUX_INITIALIZER_UNLOCKED;

CAN_device_t CAN_cfg;
Can can;
CAN_frame_t frame;
BluetoothSerial SerialBT;
__AesB64 aesB64;
Active_Can Active;
